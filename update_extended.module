<?php

/**
 * @file
 * Displays optional devel update modules in updates manager.
 */

/**
 * Implements hook_form_alter().
 */
function update_extended_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  if (user_access('administer software updates', $user)) {
    $curr_pth = current_path();

    if ($curr_pth == "admin/modules/update" || $curr_pth == "admin/reports/updates/update") {
      $projects = update_calculate_project_data(update_get_available());
      array_unshift($form['#validate'], 'update_extended_validation');

      $fstatus = $form_state['input'];

      if (count($fstatus) > 0) {
        foreach ($fstatus["projects"] as $p => $pn) {
          if (isset($fstatus["projects_extras"][$p])) {
            $form["projects"]["#value"][$p] = check_plain($p);
            $prj_vrs = $fstatus["projects_extras"][$p]["vrs"];
            $form["project_downloads"][$p]["#value"] = $projects[$p]["releases"][$prj_vrs]["download_link"];
            $form_state["values"]["project_downloads"][$p] = $projects[$p]["releases"][$prj_vrs]["download_link"];
            $form_state['input']["projects"][$p] = check_plain($p);
            $_POST['projects'][$p] = check_plain($p);
          }
        }
      }

      // Checks if there is any projects that may have two release versions.
      // It compares the $form['projects']['#options'] vs $projects.
      if (isset($form['projects']['#options'])) {
        $project_options = $form['projects']['#options'];
      }

      $projects_releases = (isset($project_options)) ? $project_options : array();

      $xtr = FALSE;

      foreach ($projects as $prj_name => $pro_atr) {
        if (isset($projects[$prj_name]['releases']) && count($projects[$prj_name]['releases']) > 1) {
          $projects_releases[$prj_name] = $projects[$prj_name];
          $vrs_times = $projects[$prj_name]['datestamp'];
          foreach($projects[$prj_name]['releases'] as $rk => $rv){
            //-Debug: dpm($prj_name .": ". date('m/d/y h:i:s A', $vrs_times)." / ". date('m/d/y h:i:s A',$rv['date']) );
            if ($vrs_times < $rv['date']) {
              //-Debug: $xtr = $prj_name."-".$rk;
              $xtr = TRUE;
            }
          }
        }
      }

      // If there are multiple releases it alterates the form.
      // -- modify the form options.
      if ($xtr == TRUE) {

        drupal_add_css(drupal_get_path('module', 'update_extended') . "/css/update_extended.css", array('type' => 'file'));

        if(!isset($form['projects'])){
          $form['projects'] = array(
            '#type' => 'tableselect',
            '#header' => array(
                          'title' => array('Data' => 'Name', 'class' => array('update-project-name')),
                          'installed_version' => 'Installed version',
                          'recommended_version' => 'Recommended version'
                        ),
            '#prefix' => '<h2>Extra Updates:</h2>'
          );

          $form['actions'] = array(
            '#type' => 'actions',
            'submit' => array(
                        '#type' => "submit",
                        '#value' => "Download these updates"
                      )
          );
        }

        // -- Create new column in table -> "Other Versions".
        $form['projects']['#header']["latest_dev"] = "Other Version";

        // Remove more than one Extras. Keep Latest.
        foreach ($projects_releases as $prj_name => $pro_atr) {
          $extra_ver = 0;
          if(isset($projects[$prj_name]['releases'])){
            foreach ($projects[$prj_name]['releases'] as $rkey => $rvalues) {
              if($rvalues["version"] != $projects[$prj_name]["recommended"]){
                if($rvalues["date"] > $projects[$prj_name]["datestamp"]){
                  if($extra_ver == 0){
                    $extra_ver = $rkey;
                  }elseif($rvalues["date"] > $projects[$prj_name]['releases'][$extra_ver]['date']){
                    unset(
                      $projects[$prj_name]['releases'][$extra_ver],
                      $projects_releases[$prj_name]['releases'][$extra_ver]
                    );
                    $extra_ver = $rkey;
                  }else{
                    unset(
                      $projects[$prj_name]['releases'][$rkey],
                      $projects_releases[$prj_name]['releases'][$rkey]
                    );
                  }
                }else{
                  unset(
                    $projects[$prj_name]['releases'][$rkey],
                    $projects_releases[$prj_name]['releases'][$rkey]
                  );
                }
              }
            }
          }
        }


        // -- now let's go through it all.
        foreach ($projects_releases as $prj_name => $pro_atr) {

          // -- Run thru all the releases available in the projects.
          // -- check all release dates to compare later int he code.
          $vrs_times = $projects[$prj_name]['datestamp'];
          $installed_date = $vrs_times;
          $slctd = $projects[$prj_name]['recommended'];
          $vers_mj = $projects[$prj_name]['existing_major'];
          if(isset($projects[$prj_name]['releases'])){
            foreach ($projects[$prj_name]['releases'] as $rkey => $rvalues) {
              if ($rvalues['date'] > $vrs_times) {
                if ($rvalues['version_major'] === $vers_mj) {
                  $slctd = $rkey;
                }
                $vrs_times = $rvalues['date'];
              }
            }
          }

          //-DEBUG: dpm($prj_name .": ". date('m/d/y h:i:s A', $vrs_times)." / ". date('m/d/y h:i:s A',$projects[$prj_name]['datestamp']) );
          // Check if(isntalled module date > new date)
          if ($vrs_times > $projects[$prj_name]['datestamp']) {

            // Add title if this is only in projects.
            if (!isset($form['projects']['#options'][$prj_name]['title'])) {
              $name = '<a href="https://www.drupal.org/project/' . $projects_releases[$prj_name]['name'] . '">' . $projects_releases[$prj_name]['info']['name'] . '</a>';
              $form['projects']['#options'][$prj_name]['title'] = filter_xss_admin($name);
            }

            $release_date = '<div class="vrs_date">Release Date:<br>' . date('m/d/y h:i:s A', $projects[$prj_name]['datestamp']) . '</div>';
            if (isset($form['projects']['#options'][$prj_name]['existing_version'])) {
              $form['projects']['#options'][$prj_name]['installed_version'] .= $form['projects']['#options'][$prj_name]['installed_version'] . $release_date;
            }
            else {
              $form['projects']['#options'][$prj_name]['installed_version'] = $projects[$prj_name]['existing_version'] . $release_date;
            }

            unset($rkey, $rvalues);

            $pick_recommended = false;

            if(isset($projects[$prj_name]['releases'])){
              foreach ($projects[$prj_name]['releases'] as $rls_vrs => $rls_atr) {

                if ((count($projects[$prj_name]['releases']) > 1) && ($rls_vrs == $projects[$prj_name]['recommended'])) {
                  // Check for date differences, description as per:
                  $recommended = (!isset($form['projects']['#options'][$prj_name]['recommended_version'])) ? $projects[$prj_name]['recommended'] : $form['projects']['#options'][$prj_name]['recommended_version'];
                  //if ($installed_date == $rls_atr['date']) {
                  if ($projects[$prj_name]['existing_version'] == $projects[$prj_name]['recommended']) {
                    $recommended = "&lsaquo; Already Installed";
                    //$recommended .= "<br>" . $rls_vrs . " | " . $projects[$prj_name]['recommended'];
                  }
                  elseif ($installed_date > $rls_atr['date']) {
                    $recommended = $recommended . '<div class="vrs_date">(Older version)<br>Release Date:<br>' . check_plain(date('m/d/y h:i:s A', $rls_atr['date'])) . '</div>';
                  }
                  else {
                    $recommended = $recommended . '<div class="vrs_date">Release Date:<br>' . check_plain(date('m/d/y h:i:s A', $rls_atr['date'])) . '</div>';
                  }

                  $this_field = array(
                    '#title' => t('Newsletter Alert'),
                    '#type' => 'radio',
                    '#attributes' => array(
                      'name' => 'projects_extras[' . $prj_name . '][vrs]',
                      'class' => array('no_before'),
                      'value' => $rls_vrs,
                    ),
                    // '#description' => "",
                    '#title' => filter_xss_admin($recommended),
                  );

                  // Check recomm. date > installed.. if not ~
                  //if (($slctd == $rls_vrs) && ($installed_date < $rls_atr['date'])) {
                    //
                  if ($projects[$prj_name]['existing_version'] != $projects[$prj_name]['recommended'] && $installed_date < $rls_atr['date']) {
                    $this_field['#attributes']['checked'] = 'true';
                    $pick_recommended = true;
                  }
                  elseif ($projects[$prj_name]['existing_version'] == $projects[$prj_name]['recommended']) {
                    $this_field['#attributes']['disabled'] = 'true';
                    array_push($this_field['#attributes']['class'], 'disabled_input');
                  };
                  $this_field = drupal_render($this_field);
                  $form['projects']['#options'][$prj_name]['recommended_version'] = $this_field;
                }
                // If other than recommended
                elseif ($rls_vrs != $projects[$prj_name]['recommended']) {

                  $this_field = array(
                    '#title' => t('Newsletter Alert'),
                    '#type' => 'radio',
                    '#attributes' => array(
                      'name' => 'projects_extras[' . $prj_name . '][vrs]',
                      'class' => array('no_before'),
                      'value' => $rls_vrs,
                    ),
                    // '#description' => "",
                    '#title' => check_plain($rls_vrs) . ' <a href="' . check_plain($projects[$prj_name]['releases'][$rls_vrs]['release_link']) . '" target="_blank" >(Release notes)</a>',
                  );

                  // -- Add extras to title --
                  $waring_message = "";
                  if ($vers_mj < $rls_atr['version_major']) {
                    $waring_message = t('<div title="Major upgrade warning" class="update-major-version-warning">-- MAJOR VERSION WARNING --<br>This update is a major version update which means that it may not be backwards compatible with your currently running version.  It is recommended that you read the release notes before proceeding at your own risk.</div>');
                    $this_field['#title'] .= $waring_message;
                  }

                  if ($waring_message == "" && $projects[$prj_name]['recommended'] == $projects[$prj_name]['existing_version']) {
                    $waring_message = t('<div title="Major upgrade warning" class="update-major-version-warning">-- NEWER UPDATE WARNING --<br> This is a newer update to this module but it has not been released as a "recommended" version. It is recommended that you read the release notes before proceeding at your own risk.</div>');
                    $this_field['#title'] .= $waring_message;
                  }

                  $this_field['#title'] .= '<div class="vrs_date">Release Date:<br>' . check_plain(date('m/d/y h:i:s A', $rls_atr['date'])) . '</div>';

                  if ($slctd == $rls_vrs && $pick_recommended == false) {
                    if ($projects[$prj_name]['recommended'] != $projects[$prj_name]['existing_version']) {
                      $this_field['#attributes']['checked'] = 'true';
                    }
                  };
                  $this_field = drupal_render($this_field);
                  $form['projects']['#options'][$prj_name]['latest_dev'] = $this_field;
                }
                // -- standard updates :
                else {
                  $form['projects']['#options'][$prj_name]['latest_dev'] = "";

                  if ($projects[$prj_name]['existing_version'] == $projects[$prj_name]['recommended']) {
                    // Checks dates, isntalled and new recomm, description as per :
                    $recommended = '<div class="vrs_date">Release Date:<br>' . date('m/d/y h:i:s A', $rls_atr['date']) . '</div>';

                    if ($projects[$prj_name]['existing_version'] == $projects[$prj_name]['recommended']) {
                      $recommended = "&lsaquo; Already Installed";
                    }
                    elseif ($installed_date > $rls_atr['date']) {
                      $recommended = '<div class="vrs_date">(Older version)<br>' . $recommended;
                    }

                    $form['projects']['#options'][$prj_name]['recommended_version'] .= $recommended;
                  }
                }

              }
            }

            $pick_recommended = false; //<-- restes the recommended to false for next project.

          }
        }
      }
    }
  }
}

/**
 * Implement validate submited form.
 */
function update_extended_validation(&$form, &$form_state) {

  global $user;
  if (user_access('administer software updates', $user)) {

      $projects = update_calculate_project_data(update_get_available());
      $fstatus = $form_state['input'];

      if (count($fstatus) > 0) {
        foreach ($fstatus["projects"] as $p => $pn) {

          // Version selected in form? if not ignore.
          if (!isset($form_state["values"]["project_downloads"][$p]) &&  !isset($fstatus["projects_extras"][$p])) {
            $form_state['input']['projects'][$p] = NULL;
            $form_state['values']['projects'][$p] = 0;
            unset($fstatus["projects"][$p]);
          }

          // Version selected in form? adjust download link.
          if (isset($fstatus["projects_extras"][$p])) {
            $form["projects"]["#value"][$p] = $p;
            $prj_vrs = $fstatus["projects_extras"][$p]["vrs"];
            $form["project_downloads"][$p]["#value"] = $projects[$p]["releases"][$prj_vrs]["download_link"];
            $form_state["values"]["project_downloads"][$p] = $projects[$p]["releases"][$prj_vrs]["download_link"];
            $form_state['input']["projects"][$p] = $p;
            $_POST['projects'][$p] = $p;
          }
        }
        if (isset($form["projects"]["#value"]["Array"])) {
          unset($form["projects"]["#value"]["Array"]);
        }
        unset($prj_vrs, $p, $pn, $projects, $form_state['values']["projects"]["Array"], $fstatus);
      }

      //Debug: form_set_error('ER','test');

      return TRUE;
  }

}
