README.txt
==========

- Provides alternative updates. It provides extra options for updating
projects when other available releases area available for updates.

- Defaults to latest version.

It defaults to the module with the latest date unless it is a major
release. It is also very helpful when -dev or newer module versions
are installed in your system but the recommended version is an older
version. it will give you an option and defaults to a newer version of
the project instead of regressing to an older recommended version of
the module. Adds extra visual clues. First version adds extra form
fields and dates and notes to make a better decision of what module
version to update.

* Notes: This module only runs when there is more than one release
  available for any module.

* Warning: Installing modules other than recommended modules could
* compromise the security of your system. If you do, please take all
* security measures to insure your new installations are clean. Run
* this module at your own discretion.
